#!/bin/sh
# install script for ameco
# mintezpresso

# Change this mode to control where ameco the script is installed
# Options: user/system
mode="user"

mkdir -p $HOME/.config/ameco $HOME/.cache/mpv
cp config $HOME/.config/ameco/config

touch $HOME/.cache/mpv/volvar $HOME/.cache/mpv/targetname
printf 25 > $HOME/.cache/mpv/volvar

case $mode in
    user ) mkdir $HOME/.local/bin
        cp ameco $HOME/.local/bin/ameco ;;
    system ) sudo cp ameco /usr/bin/ameco
esac

printf "Base install complete. Please take a look at extra for optional dwmblocks modules.\n"
