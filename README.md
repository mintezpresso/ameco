# (a)daptive (me)dia (co)ntroller
Made for mpv, mpd and spotifyd

firefox coming later, eta unknown

## Dependency

- Core:
    - spotfyd, spotify-tui
    - mpd, mpc, ncmpcpp
    - mpv, socat, jq, GNU sed
    - dunst
    - pipewire, pipewire-pulse, wireplumber, pamixer (wpctl replacement being looked at)
    - a terminal + a text editor (I use st + nvim)
- Optional, but recommended:
    - [dwmblocks](https://github.com/torrinfail/dwmblocks) (I use [dwmblock-async](https://github.com/UtkarshVerma/dwmblocks-async) )
    - sb-mpdup
    - sb-mpv
    - sb-vol


## Explain
Basically, if either mpd or mpv is on, it will act as a toggle for that one player

If both are on, ameco will only let 1 play at a time, meaning swapping playing status between them, pause mpd to play mpv and vice versa

`ameco pause` will pause all playing media, include firefox youtube

**Too sleepy to write anything else, continue later**

## Install guide
- Just read install.sh
- Once you're done reading, do ``./install.sh``
